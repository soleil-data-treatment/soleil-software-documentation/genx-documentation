# Aide et ressources de Genx pour Synchrotron SOLEIL

[<img src="https://genx.sourceforge.io/doc/_static/logo.png" width="150"/>](https://genx.sourceforge.io/doc/index.html)

## Résumé

- Fit de réflectivité
- Open source

## Sources

- Code source: https://genx.sourceforge.io
- Documentation officielle: https://genx.sourceforge.io/doc/

## Navigation rapide

| Tutoriaux |
| - |
| [Tutoriel d'installation officiel](https://genx.sourceforge.io/doc/install.html#from-source) |
| [Tutoriaux officiels](https://genx.sourceforge.io/doc/tutorials.html) |

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: texte (theta,  qz,  intensité) + paramètres de la ligne
- en sortie: texte (paramètres de fit + courbes)
- sur un disque dur
